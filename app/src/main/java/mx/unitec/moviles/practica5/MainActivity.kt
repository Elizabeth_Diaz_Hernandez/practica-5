package mx.unitec.moviles.practica5

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private var almacenamiento = "INTERNO"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sprAlmacenamiento.onItemSelectedListener = this

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.almacenamiento_array,
            android.R.layout.simple_spinner_item
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        sprAlmacenamiento.adapter = adapter
    }

    fun saveFile(v: View) {
        val file = etxtFile.text.toString()
        val data = etxtData.text.toString()
        if (almacenamiento == "INTERNO") {
            this.openFileOutput(file, Context.MODE_PRIVATE).use {
                it.write(data.toByteArray())
            }
        } else if (almacenamiento == "EXTERNO") {
            if (!ESHelper.isWritable()) {
                Toast.makeText(this, "no hay almacenamiento externo", Toast.LENGTH_LONG).show()
                return
            }
            val archivo = File(this.getExternalFilesDir(""), file)
            archivo.writeBytes(data.toByteArray())
            Toast.makeText(this, getExternalFilesDir("").toString(), Toast.LENGTH_LONG).show()
        }
    }

    fun viewFile(v: View) {
        val file = etxtFile.text.toString()

        if (almacenamiento == "INTERNO") {
            this.openFileInput(file).use {
                val text = it.bufferedReader().use {
                    it.readText()
                }

                etxtData.setText(text)
            }
        }
        else if(almacenamiento =="EXTERNO"){
            if(!ESHelper.isReadable()) {
                Toast.makeText(this,"no hay almacenamiento externo", Toast.LENGTH_LONG). show()
                return
            }
            val archivo = File(this.getExternalFilesDir(""), file)
            val texto= archivo.bufferedReader().use {
                it.readText()
            }
            etxtData.setText(texto)
            Toast.makeText(this, getExternalFilesDir("").toString(), Toast.LENGTH_LONG).show()
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        almacenamiento = parent?.getItemAtPosition(position).toString()

    }
}
